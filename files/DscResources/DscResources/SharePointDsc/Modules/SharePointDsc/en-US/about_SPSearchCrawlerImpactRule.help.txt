﻿.NAME
    SPSearchCrawlerImpactRule

# Description
    
    **Type:** Distributed
    **Requires CredSSP:** No
    
    This resource is responsible for managing the search crawl impact rules in the
    search service application. You can create new rules, change existing rules and
    remove existing rules.
    
    The default value for the Ensure parameter is Present. When you omit this
    parameter the crawl rule is created.
    
.PARAMETER ServiceAppName
    Key - String
    Search Service Application Name

.PARAMETER Name
    Key - String
    The Site for the crawl impact rule

.PARAMETER Behavior
    Read - String
    The Behavior (RequestLimit or WaitTime) for this crawl impact rule

.PARAMETER RequestLimit
    Write - UInt32
    The RequestLimit setting for the crawl impact rule

.PARAMETER WaitTime
    Write - UInt32
    The WaitTime setting for the crawl impact rule

.PARAMETER Ensure
    Write - String
    Allowed values: Present, Absent
    Ensure the crawl rule is Present or Absent

.PARAMETER InstallAccount
    Write - String
    POWERSHELL 4 ONLY: The account to run this resource as, use PsDscRunAsCredential if using PowerShell 5


