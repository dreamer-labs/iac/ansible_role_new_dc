﻿.NAME
    SPProjectServerGroup

# Description
    
    **Type:** Distributed
    **Requires CredSSP:** No
    
    This resource is used to configure a group within Project Server. This is only
    available for use when the site is configured to use Project Server permissions
    mode and for Project Server 2016 only.
    
.PARAMETER Url
    Key - string
    The default zone URL of the Project site to manage the group at

.PARAMETER Name
    Key - string
    The name of the group

.PARAMETER Description
    Write - string
    The description of the group

.PARAMETER ADGroup
    Write - string
    What AD group should be used to synchronise membership to this Project Server group, cannot be used with Members, MembersToInclude or MembersToExclude

.PARAMETER Members
    Write - string
    A fixed list of members to be in this group, cannot be used with ADGroup, MembersToInclude or MembersToExclude

.PARAMETER MembersToInclude
    Write - string
    A list of members to ensure are in this group, cannot be used with ADGroup or Members

.PARAMETER MembersToExclude
    Write - string
    A list of members to ensure are not in this group, cannot be used with ADGroup or Members

.PARAMETER Ensure
    Write - string
    Allowed values: Present, Absent
    Present if the service app should exist, absent if it should not

.PARAMETER InstallAccount
    Write - String
    POWERSHELL 4 ONLY: The account to run this resource as, use PsDscRunAsCredential if using PowerShell 5


.EXAMPLE
    This example shows how to create a group with a specific list of members in a PWA site


Configuration Example 
{
    param(
        [Parameter(Mandatory = $true)]
        [PSCredential]
        $SetupAccount
    )
    Import-DscResource -ModuleName SharePointDsc

    node localhost 
    {
        SPProjectServerGroup Group
        {
            Url = "http://projects.contoso.com"
            Name = "My group"
            Members = @(
                "Domain\User1"
                "Domain\User2"
            )
            PSDscRunAsCredential = $SetupAccount
        }
    }
}


.EXAMPLE
    This example shows how to create a PWA group mapped to a group in AD


Configuration Example 
{
    param(
        [Parameter(Mandatory = $true)]
        [PSCredential]
        $SetupAccount
    )
    Import-DscResource -ModuleName SharePointDsc

    node localhost 
    {
        SPProjectServerGroup Group
        {
            Url = "http://projects.contoso.com"
            Name = "My group"
            ADGroup = "Domain\Group"
            PSDscRunAsCredential = $SetupAccount
        }
    }
}


